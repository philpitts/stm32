#ifndef LOOP_BLINKER_H
#define LOOP_BLINKER_H

#include "stm32f0xx.h"

void blink(void);
void delay(const int d);

#endif