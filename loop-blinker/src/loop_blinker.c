#include "loop_blinker.h"

void blink(void)
{
	// GPIOA Periph clock enable
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

	// PA0, PA1, PA3 in output mode
	GPIOA->MODER |= (GPIO_MODER_MODER0_0);
	GPIOA->MODER |= (GPIO_MODER_MODER1_0);
	GPIOA->MODER |= (GPIO_MODER_MODER3_0);

	// Push pull mode selected
	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_0);
	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_1);
	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_3);

	// Maximum speed setting
	GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR0);
	GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR1);
	GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR3);

	// Pull-up and pull-down resistors disabled
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR0);
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR1);
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR3);

	while(1)
	{
		// Set PA0 and delay ~ 0.25 sec.
		GPIOA->BSRR = GPIO_BSRR_BS_0;
		delay(SystemCoreClock / 32);

		// Reset PA0, set PA1, delay ~ 0.25 sec.
		GPIOA->BSRR = GPIO_BSRR_BR_0;
		GPIOA->BSRR = GPIO_BSRR_BS_1;
		delay(SystemCoreClock / 32);

		// Reset PA1, set PA3, delay ~ 0.25 sec.
		GPIOA->BSRR = GPIO_BSRR_BR_1;
		GPIOA->BSRR = GPIO_BSRR_BS_3;
		delay(SystemCoreClock / 16);

		// Reset PA3
		GPIOA->BSRR = GPIO_BSRR_BR_3;
	}
}

void delay(const int d)
{
	volatile int i;
 	for (i = d; i > 0; i--){ ; } 
	return;
}