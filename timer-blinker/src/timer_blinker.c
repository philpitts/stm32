#include "timer_blinker.h"

void blink(void) {
    TIM_TimeBaseInitTypeDef timer;
    NVIC_InitTypeDef nvic;

    // GPIOA Periph clock enable
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

	// Initialize PA0 in output mode
	GPIOA->MODER |= (GPIO_MODER_MODER0_0);      // output mode
	GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_0);       // Push pull mode
	GPIOA->OSPEEDR |= (GPIO_OSPEEDER_OSPEEDR0); // Maximum speed setting
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR0);       // Pull-up / pull-down disabled

    // Enable TIM3 clock
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

    // Set up the timer to fire an interrupt every second
    //      TIM_Period: Fire interrupt every 1000 steps
    //      TIM_Prescaler: Use the function below to step every 1kHz
    //                     prescaler = (SystemCoreClock / Hz) - 1
    timer.TIM_CounterMode = TIM_CounterMode_Up;
    timer.TIM_Period = 1000 - 1;
    timer.TIM_Prescaler = (uint16_t)((SystemCoreClock / 1000) - 1);

    // Perform the initialization
    TIM_TimeBaseInit(TIM3, &timer);

    // Allow TIM3 to generate global interrupts
    nvic.NVIC_IRQChannel = TIM3_IRQn;
    nvic.NVIC_IRQChannelPriority = 0;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);

    // Enable the interrupt itself
    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

    // Enable the timer counter
    TIM_Cmd(TIM3, ENABLE);
}